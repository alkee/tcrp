﻿using System.Diagnostics;
using System.Threading;
using tcrp;
using tcrp.Sample;

namespace tcrpTest.IssueTest
{
    static class Issue25
    {
        static void ChangeDispatcherByMessageInBackground()
        {
            // dispatchers
            var clientDispatcher = new SampleSwitchService();

            // server
            using (Acceptor acceptor = new Acceptor())
            {
                acceptor.Connected += (s) =>
                {
                    s.Send(new SampleMessage { });
                    s.Send(new SampleMessage2 { });
                };
                acceptor.Start(Config.PORT);

                var client = new Connector();
                client.Connected += (s, ok) =>
                {
                    Debug.Assert(ok);
                    s.MessageDispatcher.AddContainer(clientDispatcher);
                };

                Debug.Assert(clientDispatcher.ReceivedCount == 0);
                Debug.Assert(clientDispatcher.Received2Count == 0);
                Debug.Assert(clientDispatcher.NextDispatcher.ReceivedCount == 0);
                Debug.Assert(clientDispatcher.NextDispatcher.Received2Count == 0);

                clientDispatcher.NextDispatcher.SignalWaitable(); // 최종적으로 대기할 dispatcher
                client.Connect(Config.TEST_TARGET_HOST, Config.PORT);
                clientDispatcher.NextDispatcher.Wait();

                Debug.Assert(clientDispatcher.ReceivedCount == 1);
                Debug.Assert(clientDispatcher.Received2Count == 0); // 제대로 삭제되지 않으면 assert
                Debug.Assert(clientDispatcher.NextDispatcher.ReceivedCount == 0);
                Debug.Assert(clientDispatcher.NextDispatcher.Received2Count == 1);
            }
        }

        static void ChangeDispatcherByMessageOnManualPolling()
        {
            // dispatchers
            var clientDispatcher = new SampleSwitchService();

            // server
            using (Acceptor acceptor = new Acceptor())
            {
                acceptor.Connected += (s) =>
                {
                    Thread.Sleep(100); // TODO: #27 https://bitbucket.org/alkee/tcrp/issues/27
                    s.Send(new SampleMessage { });
                    s.Send(new SampleMessage2 { });
                };
                acceptor.Start(Config.PORT);

                var client = new Connector(false);
                client.Connected += (s, ok) =>
                {
                    Debug.Assert(ok);
                    s.MessageDispatcher.AddContainer(clientDispatcher);
                };

                Debug.Assert(clientDispatcher.ReceivedCount == 0);
                Debug.Assert(clientDispatcher.Received2Count == 0);
                Debug.Assert(clientDispatcher.NextDispatcher.ReceivedCount == 0);
                Debug.Assert(clientDispatcher.NextDispatcher.Received2Count == 0);

                client.Connect(Config.TEST_TARGET_HOST, Config.PORT);
                while (client.Poll() == false) Thread.Sleep(1); // wait for connect
                while (client.Poll() == false) Thread.Sleep(1); // wait for Message1
                while (client.Poll() == false) Thread.Sleep(1); // wait for Message2

                Debug.Assert(clientDispatcher.ReceivedCount == 1);
                Debug.Assert(clientDispatcher.Received2Count == 0);
                Debug.Assert(clientDispatcher.NextDispatcher.ReceivedCount == 0);
                Debug.Assert(clientDispatcher.NextDispatcher.Received2Count == 1);

            }
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;

namespace tcrp
{
    public abstract class SerialNumbered
    {
        private int serial = 0;
        public int Serial { get; private set; }
        public SerialNumbered()
        {
            Serial = Interlocked.Increment(ref serial);
        }
    }

    public class Swappable<T> where T : class
    {
        public Swappable(T reference) { r = reference; }
        public T GetRef() { lock (this) return r; }
        public virtual T Swap(T newReference)
        {
            lock (this)
            {
                var old = r;
                r = newReference;
                return old;
            }
        }
        private T r;
    }

    public class BackgroundWorker<T> : IDisposable where T : class
    {
        public BackgroundWorker(int numberOfThread)
        {
            contexts = new List<Context>();
            for (var i = 0; i < numberOfThread; ++i)
            {
                var context = new Context { Worker = new Thread(ThreadMain), Index = i };
                context.Worker.Start(context);
                contexts.Add(context);
            }
        }

        public event Action<int/*threadIndex*/, T> Dequeued;

        public int WorkerCount { get { return contexts.Count; } }

        public void Enqueue(int index, T workMessage)
        {
            if (index >= contexts.Count) throw new ArgumentOutOfRangeException("index");
            var c = contexts[index];
            if (c.ShouldTerminate) return; // or throw ? 어차피 queue 에 넣어도 동작할 thread 가 없다.
            var rq = c.ReplacableQueue;
            lock (rq) rq.GetRef().Enqueue(workMessage);
            c.WaitHandle.Set();
        }

        // *주의* 비용이 너무 커 보인다.
        public void MoveElements(int indexBefore, int indexAfter, Predicate<T> condition)
        {
            if (condition == null) throw new ArgumentNullException("condition");
            var before = contexts[indexBefore];
            var removedQueue = new Queue<T>(); // condition 에 맞는 데이터가 제거된 before queue

            lock (before.ReplacableQueue)
            {
                var q = before.ReplacableQueue.GetRef();
                while (q.Count > 0)
                {
                    var e = q.Dequeue();
                    if (condition(e))
                    {
                        Enqueue(indexAfter, e);
                    }
                    else
                    {
                        removedQueue.Enqueue(e);
                    }
                }
                before.ReplacableQueue.Swap(removedQueue);
            }
        }

        public void ThreadMain(object conextObj)
        {
            var context = conextObj as Context;
            while (context.ShouldTerminate == false)
            {
                var q = context.ReplacableQueue.Swap(new Queue<T>());
                if (q.Count == 0)
                {
                    context.WaitHandle.WaitOne();
                }
                else
                {
                    while (q.Count > 0)
                    {
                        var w = q.Dequeue();
                        if (Dequeued != null) Dequeued(context.Index, w);
                    }
                }
            }
        }

        public void Dispose()
        {
            // 강제로 terminate 하면 남아있는 queue 를 처리할 수 없으므로, 모두 처리하고 종료하도록 signal 전달
            foreach (var c in contexts)
            {
                c.ShouldTerminate = true;
                c.WaitHandle.Set();
            }

            // 하나의 foreach 안에서 signal 전달 및 대기도 할 수 있지만, 미리 signal 을 주면 동시에 thread 가 종료될 수 있으므로
            // 좀 더 빠른 종료를 위해 분리
            foreach (var c in contexts) if (c.Worker.IsAlive) c.Worker.Join();

        }

        private class Context
        {
            public Swappable<Queue<T>> ReplacableQueue = new Swappable<Queue<T>>(new Queue<T>());
            public Thread Worker;
            public int Index;
            public EconomicResetEvent WaitHandle = new EconomicResetEvent(false); // http://www.liranchen.com/2010/08/reducing-autoresetevents.html
            public bool ShouldTerminate = false;
        }

        private List<Context> contexts;
    }

    // AutoResetEvent 성능이슈(http://blog.teamleadnet.com/2012/02/why-autoresetevent-is-slow-and-how-to.html)로
    // 개선된 ResetEvent http://www.liranchen.com/2010/08/reducing-autoresetevents.html
    // 특히나 BackgroundWorker 의 경우 중복실행되는 Set(by Enqueue) call 이 많을 것이기 때문에 효과적
    public class EconomicResetEvent
    {
        private volatile int eventState;
        private AutoResetEvent waitHandle;

        private const int EVENT_SET = 1;
        private const int EVENT_NOT_SET = 2;
        private const int EVENT_ON_WAIT = 3;

        public EconomicResetEvent(bool initialState)
        {
            waitHandle = new AutoResetEvent(initialState);
            eventState = initialState ? EVENT_SET : EVENT_NOT_SET;
        }

        public void WaitOne()
        {
            if (eventState == EVENT_SET && Interlocked.CompareExchange(
                ref eventState, EVENT_NOT_SET, EVENT_SET) == EVENT_SET)
            {
                return;
            }

            if (eventState == EVENT_NOT_SET && Interlocked.CompareExchange(
                ref eventState, EVENT_ON_WAIT, EVENT_NOT_SET) == EVENT_NOT_SET)
            {
                waitHandle.WaitOne();
            }
        }

        public void Set()
        {
            if (eventState == EVENT_NOT_SET && Interlocked.CompareExchange(
                ref eventState, EVENT_SET, EVENT_NOT_SET) == EVENT_NOT_SET)
            {
                return;
            }

            if (eventState == EVENT_ON_WAIT && Interlocked.CompareExchange(
                ref eventState, EVENT_NOT_SET, EVENT_ON_WAIT) == EVENT_ON_WAIT)
            {
                waitHandle.Set();
            }
        }
    }

    public static class Parser
    {
        // from http://stackoverflow.com/questions/2727609/best-way-to-create-ipendpoint-from-string
        // Handles IPv4 and IPv6 notation.
        public static IPEndPoint ParseIPEndPoint(string endpointstring)
        {
            return ParseIPEndPoint(endpointstring, -1);
        }

        public static IPEndPoint ParseIPEndPoint(string endpointstring, int defaultport)
        {
            if (string.IsNullOrEmpty(endpointstring)
                || endpointstring.Trim().Length == 0)
            {
                throw new ArgumentException("Endpoint descriptor may not be empty.");
            }

            if (defaultport != -1 &&
                (defaultport < IPEndPoint.MinPort
                || defaultport > IPEndPoint.MaxPort))
            {
                throw new ArgumentException(string.Format("Invalid default port '{0}'", defaultport));
            }

            string[] values = endpointstring.Split(new char[] { ':' });
            IPAddress ipaddy;
            int port = -1;

            //check if we have an IPv6 or ports
            if (values.Length <= 2) // ipv4 or hostname
            {
                if (values.Length == 1)
                    //no port is specified, default
                    port = defaultport;
                else
                    port = getPort(values[1]);

                //try to use the address as IPv4, otherwise get hostname
                if (!IPAddress.TryParse(values[0], out ipaddy))
                    ipaddy = ParseIpAddress(values[0]);
            }
            else if (values.Length > 2) //ipv6
            {
                //could [a:b:c]:d
                if (values[0].StartsWith("[") && values[values.Length - 2].EndsWith("]"))
                {
                    string ipaddressstring = string.Join(":", values.Take(values.Length - 1).ToArray());
                    ipaddy = IPAddress.Parse(ipaddressstring);
                    port = getPort(values[values.Length - 1]);
                }
                else //[a:b:c] or a:b:c
                {
                    ipaddy = IPAddress.Parse(endpointstring);
                    port = defaultport;
                }
            }
            else
            {
                throw new FormatException(string.Format("Invalid endpoint ipaddress '{0}'", endpointstring));
            }

            if (port == -1)
                throw new ArgumentException(string.Format("No port specified: '{0}'", endpointstring));

            return new IPEndPoint(ipaddy, port);
        }

        private static int getPort(string p)
        {
            int port;

            if (!int.TryParse(p, out port)
             || port < IPEndPoint.MinPort
             || port > IPEndPoint.MaxPort)
            {
                throw new FormatException(string.Format("Invalid end point port '{0}'", p));
            }

            return port;
        }

        public static IPAddress ParseIpAddress(string p, bool preferIpv4 = true)
        {
            var hosts = Dns.GetHostAddresses(p);

            if (hosts == null || hosts.Length == 0)
                throw new ArgumentException(string.Format("Host not found: {0}", p));

            
            if (preferIpv4)
                foreach (var h in hosts)
                    if (h.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        return h;

            return hosts[0];
        }
    }

    // [CountDownEvent]((https://msdn.microsoft.com/library/system.threading.countdownevent.aspx) 흉내
    public abstract class CountdownWaitEvent
    {
        public CountdownWaitEvent()
        {
            waitHandle = new ManualResetEvent(false);
        }

        public CountdownWaitEvent(EventWaitHandle waitHandle)
        {
            this.waitHandle = waitHandle;
        }

        public void SignalContinuable()
        {
            if (counter == 0) return;
            var count = Interlocked.Decrement(ref counter);
            if (count == 0) waitHandle.Set();
        }

        public void SignalWaitable(int count = 1)
        {
            if (count < 1) throw new ArgumentException("must be larger than 0", "count");
            Interlocked.Exchange(ref counter, count);
            waitHandle.Reset();
        }

        public void Wait()
        {
            waitHandle.WaitOne();
        }

        private EventWaitHandle waitHandle; // CountdownEvent  class 를 사용할 수 있다면 좋았겠지만, .NET 4.0 부터 지원
        private int counter;
    }
}

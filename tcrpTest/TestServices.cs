﻿using System.Threading;
using tcrp;
using tcrp.Sample;

namespace tcrpTest
{
    class SampleSwitchService : CountdownWaitEvent
    {
        public int ReceivedCount = 0;
        public int Received2Count = 0;
        public SampleSwitchNextService NextDispatcher;

        public SampleSwitchService()
        {
            NextDispatcher = new SampleSwitchNextService();
        }

        public SampleSwitchService(EventWaitHandle waitHandle) : base(waitHandle)
        {
            NextDispatcher = new SampleSwitchNextService(waitHandle);
        }

        [MessageDispatcher]
        public void Dispatch(Session s, SampleMessage packet)
        {
            ++ReceivedCount;
            s.MessageDispatcher.RemoveContainer(this);
            s.MessageDispatcher.AddContainer(NextDispatcher);
        }

        [MessageDispatcher]
        public void Dispatch(Session s, SampleMessage2 packet)
        {
            ++Received2Count;
            SignalContinuable();
        }

    }

    class SampleSwitchNextService : CountdownWaitEvent
    {
        public int ReceivedCount = 0;
        public int Received2Count = 0;

        public SampleSwitchNextService() { }
        public SampleSwitchNextService(EventWaitHandle waitHandle) : base(waitHandle) { }

        [MessageDispatcher]
        public void Dispatch(Session s, SampleMessage packet)
        {
            ++ReceivedCount;
        }

        [MessageDispatcher]
        public void Dispatch(Session s, SampleMessage2 packet)
        {
            ++Received2Count;
            SignalContinuable();
        }
    }

    class SampleCounterService : CountdownWaitEvent
    {
        public int ReceivedCount = 0;

        public SampleCounterService() { }
        public SampleCounterService(EventWaitHandle waitHandle) : base(waitHandle) { }

        [MessageDispatcher] // Session 과 protobuf message 를 parameter 로 받는 public void 형의 함수만 가능
        public void Dispatch(Session s, SampleMessage packet)
        {
            ++ReceivedCount; // MessageDispatcher 끼리(정확히는 같은 Group끼리)는 항상 thread-safe (별도의 queue 를 통해 순차적으로 실행)
            SignalContinuable();
        }
    }
}

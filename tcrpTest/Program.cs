﻿using System;
using System.Linq;
using System.Reflection;

namespace tcrpTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // 
            Console.WriteLine("testing samples");
            Console.Write("\tGettigStarted");
            tcrp.Sample.GettigStarted.Test();
            Console.WriteLine(" ... done");

            Console.Write("\tGroupping");
            tcrp.Sample.Groupping.Test();
            Console.WriteLine(" ... done");

            // issue 관련 testing - tcrpTest.IssueTest 내의 모든 Issue~ 로 시작하는 class 의 void(void) 타입의 internal static member 함수 실행
            const string targetNamespace = "tcrpTest.IssueTest";
            var targetClasses = from t in Assembly.GetExecutingAssembly().GetTypes()
                    where t.IsClass && t.Namespace == targetNamespace && t.Name.IndexOf("Issue") == 0
                    select t;

            foreach(var c in targetClasses)
            {
                Console.WriteLine("testing {0}", c.Name);

                foreach(var m in c.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.InvokeMethod))
                {
                    if (m.GetParameters().Length > 0) continue;
                    if (!m.ReturnType.Equals(typeof(void))) continue;
                    Console.Write("\t{0} ... ", m.Name);
                    m.Invoke(null, null);
                    Console.WriteLine("done");
                }
            }

            Console.WriteLine("\r\nenter to terminate");
            Console.ReadLine();
        }
    }
}

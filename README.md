﻿# tcrp

**T**cp based **C**ommunicator with [**R**eflection](https://msdn.microsoft.com/ko-kr/library/mt656691.aspx) and [**P**rotobuf-net](https://github.com/mgravell/protobuf-net)

손쉽게 tcp message stream 을 만들고(`.proto` 이용) 이를 client/server 어느쪽에서도 쉽게 사용할 수 있도록 디자인된 라이브러리. 
루니아전기(project xrated)와 크리티카(project apple)에서는 native c++ 에서 유사한 방식의 서비스를 제공했지만, protobuf 및 c# 의 reflection 을 이용해 조금 더 단순화된 사용성을 확보할 수 있다.


## getting started

최대한 간략한 예제는 [tcrp.cs](https://bitbucket.org/alkee/tcrp/src/master/tcrp/tcrp.cs) 에 포함하도록 한다. 

 * 서버
    1. 서비스(message dispatcher 목록 및 구현)의 준비
    2. 서버(Acceptor)의 생성 + 시작 서비스(session 이 연결되면 바로 받을 수 있는 dispatcher) 등록
    3. Acceptor.Start
 
 * 클라이언트
    1. 서비스(message dispatcher 목록 및 구현)의 준비
    2. framework 의 [TcpClient](https://msdn.microsoft.com/ko-kr/library/system.net.sockets.tcpclient.aspx)를 사용해 서버에 연결
    3. TcpClient instance 를 이용해 Session 생성 및 시작 서비스를 session 에 등록

[Sample 보기](https://bitbucket.org/alkee/tcrp/src/master/tcrp/tcrp.cs)

﻿using System;
using System.Collections.Generic;

namespace tcrp
{
    internal class MessageHandler : IDisposable
    {
        public MessageHandler(int numberOfThread)
        {
            if (numberOfThread > 0)
            {
                worker = new BackgroundWorker<ReservedMessage>(numberOfThread);
                worker.Dequeued += WorkerCallback;
            }
            else mq = new Queue<ReservedMessage>(); // background thread 가 없는 경우
        }

        public void Dispose()
        {
            if (worker != null) worker.Dispose();
        }

        ~MessageHandler()
        {
            Dispose();
        }

        public int NumberOfWorkerThread { get { return worker == null ? 0 : worker.WorkerCount; } }

        public void Enqueue(Session session, object message, int distributionKey)
        {
            var rm = new ReservedMessage { Session = session, Message = message };
            if (worker!=null)
            {
                worker.Enqueue(SelectWorkerIndex(distributionKey), rm);
            }
            else
            {
                lock (mq) mq.Enqueue(rm);
            }
        }

        public void RearrangeQueue(Session session, int distributionKeyBefore, int distributionKeyAfter)
        {
            if (worker == null) return;
            var indexBefore = SelectWorkerIndex(distributionKeyBefore);
            int indexAfter = SelectWorkerIndex(distributionKeyAfter);
            if (indexBefore == indexAfter) return; // 바꿔도 같은 queue

            worker.MoveElements(indexBefore, indexAfter, (m) => m.Session == session);
        }

        public int Dequeue() // makes invoke
        {
            if (worker != null) throw new InvalidOperationException("Dequeue only works in 0 io thread mode");
            ReservedMessage rm;
            int count = 0;
            lock (mq)
            {
                if (mq.Count == 0) return 0;
                rm = mq.Dequeue();
                count = mq.Count;
            }
            rm.Session.MessageDispatcher.Invoke(rm.Session, rm.Message);
            return count;
        }

        private void WorkerCallback(int threadIndex, ReservedMessage msg)
        {
            msg.Session.MessageDispatcher.Invoke(msg.Session, msg.Message);
        }

        private int SelectWorkerIndex(int key)
        {
            return key % worker.WorkerCount; // TODO: 좀 더 좋은 분산 방법?
        }

        private class ReservedMessage
        {
            public Session Session { get; set; }
            public object Message { get; set; }
        }

        private BackgroundWorker<ReservedMessage> worker;
        private Queue<ReservedMessage> mq;
    }
}

﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading;

// Tcp based Communicator with Reflection and Protobuf-net

// 간단한 사용법/테스트 소스
namespace tcrp.Sample
{
    public static class Config
    {
        public static readonly IPAddress TEST_TARGET_HOST = Parser.ParseIpAddress("localhost");// IPAddress.Loopback;
        public const int PORT = 4684;
    }

    public static class GettigStarted
    {
        public static void Test()
        {
            // dispatchers
            var serverDispatcher = new SampleUnsafeService();
            var clientDispatcher = new SampleService();

            // server
            using (Acceptor acceptor = new Acceptor())
            {
                Session session = null;
                acceptor.Connected += (s) => {
                    s.MessageDispatcher.AddContainer(serverDispatcher);  // 연결되면 해당 session 에 대해 SampleUnsafeService 시작되도록.
                    session = s;
                    serverDispatcher.SignalContinuable();
                };
                acceptor.Start(Config.PORT);

                // client 
                var connector = new Connector();
                connector.Connected += (c, ok) => {
                    Debug.Assert(ok); // 연결이 올바르게 만들어져야 한다.
                    clientDispatcher.SignalContinuable();
                }; // 클라이언트쪽도 마찬가지로 message 처리기를 등록해 사용할 수 있다. 다른 방식은 GlobalDispatcher 예제 참고
                connector.MessageDispatcher.AddContainer(clientDispatcher);


                clientDispatcher.SignalWaitable();
                serverDispatcher.SignalWaitable();

                connector.Connect(Config.TEST_TARGET_HOST, Config.PORT); // connect to local. 
                clientDispatcher.Wait(); // 연결될때까지 기다림
                serverDispatcher.Wait(); // 서버도 연결상태가 완료될때까지 기다림
                Debug.Assert(session != null);

                Debug.Assert(serverDispatcher.ReceivedCount == 0); // 서버가 처리하기 전 값
                serverDispatcher.SignalWaitable();
                connector.Send(new SampleMessage()); // 클라이언트에서 서버로 메시지 전달
                serverDispatcher.Wait(); // 서버가 메시지 받을 때 까지 기다림
                Debug.Assert(serverDispatcher.ReceivedCount == 1); // 결과확인

                Debug.Assert(clientDispatcher.ReceivedCount == 0); // 클라이언트가 처리하기 전 값
                clientDispatcher.SignalWaitable();
                session.Send(new SampleMessage()); // 서버에서 클라이언트로 메시지 전달
                clientDispatcher.Wait(); // 클라이언트가 메시지 받을 때 까지 기다림
                Debug.Assert(clientDispatcher.ReceivedCount == 1); // 결과확인
            }
        }
    }

    public static class Groupping
    {
        public static void Test()
        {
            // dispatchers
            var serverDispatcher = new SampleService();
            var clientDispatcher = new SampleUnsafeService(); // 여러 클라이언트에서 공통으로 사용하기때문에 동기화 필요해 UnsafeService 사용

            // server
            using (Acceptor acceptor = new Acceptor())
            {
                const int CLIENT_COUNT = 5;

                var sessions = new List<Session>();
                acceptor.Connected += (s) =>
                {
                    s.MessageDispatcher.AddContainer(serverDispatcher);  // 연결되면 해당 session 에 대해 SampleUnsafeService 시작되도록.
                    lock(sessions) sessions.Add(s);
                    serverDispatcher.SignalContinuable(); // 
                };
                serverDispatcher.SignalWaitable(CLIENT_COUNT);
                acceptor.Start(Config.PORT);

                // client 
                var connectors = new List<Connector>();
                for(var i=0; i<CLIENT_COUNT; ++i)
                {
                    var connector = new Connector();
                    connector.MessageDispatcher.AddContainer(clientDispatcher);
                    connector.Connect(Config.TEST_TARGET_HOST, Config.PORT);
                }
                serverDispatcher.Wait(); // 연결이 모두 완료되기를 기다린다.
                Debug.Assert(sessions.Count == CLIENT_COUNT);

                // groupping
                acceptor.SetGroup(sessions[0]); // 0번 클라이언트는 혼자 그룹
                acceptor.SetGroup(sessions[1], sessions[2]); // 1번과 2번은 같은 그룹
                acceptor.SetGroup(sessions[3], sessions[4]); // 3번과 4번은 같은 그룹
                acceptor.RemoveGroup(sessions[4]); // 4번 그룹에서 빼기 - 3번 혼자그룹

                // test groupped state
                Debug.Assert(acceptor.HasGroup(sessions[0]) == true);
                Debug.Assert(acceptor.HasGroup(sessions[1]) == true);
                Debug.Assert(acceptor.HasGroup(sessions[2]) == true);
                Debug.Assert(acceptor.HasGroup(sessions[3]) == true);
                Debug.Assert(acceptor.HasGroup(sessions[4]) == false); // 아직 아무도 그룹에 안들어있다.

                // message to group
                Debug.Assert(clientDispatcher.ReceivedCount == 0);
                clientDispatcher.SignalWaitable(2);
                acceptor.Broadcast(sessions[2], new SampleMessage()); // 2번이 들어있는 그룹에 메시지 전달
                clientDispatcher.Wait(); // 메시지를 받기를 기다린다.
                Debug.Assert(clientDispatcher.ReceivedCount == 2); // 2명의 멤버에게 전달
            }
        }
    }

    #region sample dispatchers
    class SampleService : CountdownWaitEvent
    {
        public int ReceivedCount = 0;

        public SampleService() { }
        public SampleService(EventWaitHandle waitHandle) : base(waitHandle) { }

        [MessageDispatcher] // Session 과 protobuf message 를 parameter 로 받는 public void 형의 함수만 가능
        public void Dispatch(Session s, SampleMessage packet)
        {
            ++ReceivedCount; // MessageDispatcher 끼리(정확히는 같은 Group끼리)는 항상 thread-safe (별도의 queue 를 통해 순차적으로 실행)
            SignalContinuable();
        }
    }

    class SampleUnsafeService : CountdownWaitEvent
    {
        public int ReceivedCount = 0;
        private object syncObj = new object();

        public SampleUnsafeService() { }
        public SampleUnsafeService(EventWaitHandle waitHandle) : base(waitHandle) { }

        [UnsafeMessageDispatcher] // 어느 thread 에서든 동시에 불릴 수 있다. 안전하지 않지만 빠른 반응(MessageDispatcher 처리보다 우선)과 성능.
        public void Dispatch(Session s, SampleMessage packet)
        {
            lock (syncObj) ++ReceivedCount;
            SignalContinuable();
        }
    }
    #endregion

    #region protobuf message
    [global::System.Serializable, global::ProtoBuf.ProtoContract(Name = @"SampleMessage")]
    public partial class SampleMessage : global::ProtoBuf.IExtensible
    {
        public SampleMessage() { }

        private string _msg = "";
        [global::ProtoBuf.ProtoMember(1, IsRequired = false, Name = @"msg", DataFormat = global::ProtoBuf.DataFormat.Default)]
        [global::System.ComponentModel.DefaultValue("")]
        public string msg
        {
            get { return _msg; }
            set { _msg = value; }
        }
        private global::ProtoBuf.IExtension extensionObject;
        global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
        { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
    }

    [global::System.Serializable, global::ProtoBuf.ProtoContract(Name = @"SampleMessage2")]
    public partial class SampleMessage2 : global::ProtoBuf.IExtensible
    {
        public SampleMessage2() { }

        private string _msg2 = "";
        [global::ProtoBuf.ProtoMember(1, IsRequired = false, Name = @"msg", DataFormat = global::ProtoBuf.DataFormat.Default)]
        [global::System.ComponentModel.DefaultValue("")]
        public string msg2
        {
            get { return _msg2; }
            set { _msg2 = value; }
        }
        private global::ProtoBuf.IExtension extensionObject;
        global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
        { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
    }
    #endregion
}

﻿using System.Diagnostics;
using System.Threading;
using tcrp;
using tcrp.Sample;

namespace tcrpTest.IssueTest
{
    static class Issue27
    {
        static void SendMessageRightAfterConnected()
        {
            // dispatchers
            var clientDispatcher = new SampleCounterService();
            const int TEST_COUNT = 2;
            const int TIMEOUT_SEC = 5;

            // server
            using (Acceptor acceptor = new Acceptor())
            {
                acceptor.Connected += (s) =>
                {
                    for (var i = 0; i < TEST_COUNT; ++i) s.Send(new SampleMessage { });
                };
                acceptor.Start(Config.PORT);

                var client = new Connector(false); // manual polling
                client.Connected += (s, ok) =>
                {
                    Debug.Assert(ok);
                    s.MessageDispatcher.AddContainer(clientDispatcher);
                };

                Debug.Assert(clientDispatcher.ReceivedCount == 0);

                client.Connect(Config.TEST_TARGET_HOST, Config.PORT);
                var stamp = System.DateTime.Now.Ticks;
                while (clientDispatcher.ReceivedCount != TEST_COUNT
                    && (System.DateTime.Now.Ticks - stamp) < (System.TimeSpan.TicksPerSecond * TIMEOUT_SEC)) // timeout 내에서만
                {
                    client.Poll();
                    Thread.Sleep(1);
                }

                Debug.Assert(clientDispatcher.ReceivedCount == TEST_COUNT);
            }
        }
    }
}



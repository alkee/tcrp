﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace tcrp
{
    public class Connector : Session
    {
        public event Action<Connector, bool /*false if failed*/> Connected;
        public event Action<Connector, bool /*closed by peer*/> Disconnected;

        public Connector(bool backgroundThreadPolling = true)
        {
            MessageArrived += Session_MessageArrived;
            Closed += Session_Closed;
            bgPolling = backgroundThreadPolling;
        }

        public void Connect(string host, int port)
        {
            Connect(Parser.ParseIpAddress(host), port);
        }

        public void Connect(IPAddress host, int port)
        {
            if (channel.Connected) throw new InvalidOperationException("already connected");
            channel.BeginConnect(host, port, (ar) =>
            {
                if (channel.Connected)
                {
                    BindConnectedChannel(channel);
                    channel.EndConnect(ar);
                }
                if (bgPolling && Connected != null) Connected(this, channel.Connected);
                else if (bgPolling == false)
                {
                    var mp = new MessageProcessor { Type = channel.Connected ? MessageProcessor.MessageType.CONNECTED : MessageProcessor.MessageType.CONNECTED_FAILED };
                    lock (mq) mq.Enqueue(mp);
                }
                if (channel.Connected) BeginRead();
            }, null);
        }

        public int QueuedCount { get { lock (mq) return mq.Count; } }

        public bool Poll()
        {
            if (QueuedCount == 0) return false;
            MessageProcessor mp;
            lock (mq) mp = mq.Dequeue(); // 다른곳에서 queue 를 비우는 일이 없으므로 InvalidOperationException(비어있을 때 발생) 무시

            switch (mp.Type)
            {
                case MessageProcessor.MessageType.NORMAL:
                    if (MessageDispatcher.Invoke(this, mp.Message) == 0 && MessageDispatcher.HasDispatcher(mp.Message.GetType()))
                        throw new ProtocolNotFoundException( mp.Message.GetType().Name + " dispatcher not found");
                    break;
                case MessageProcessor.MessageType.CONNECTED:
                case MessageProcessor.MessageType.CONNECTED_FAILED:
                    if (Connected != null)
                        Connected(this, mp.Type == MessageProcessor.MessageType.CONNECTED ? true : false);
                    break;
                case MessageProcessor.MessageType.DISCONNECTED:
                case MessageProcessor.MessageType.DISCONNECTED_BYPEER:
                    if (Disconnected != null)
                        Disconnected(this, mp.Type == MessageProcessor.MessageType.DISCONNECTED_BYPEER ? true : false);
                    break;
                case MessageProcessor.MessageType.EXCEPTION:
                    throw mp.Message as Exception;
            }

            return true;
        }

        #region io thread 에서 불리는 callback
        private void Session_MessageArrived(Session session, object message)
        {
#if DEBUG || UNITY_EDITOR
            // manual polling 의 경우 dispatcher 가 메시지를 받은 이후에 변경될 수 있기 때문에
            // HasDispatcher 검사는 이곳에서 하면 안된다.
            if (bgPolling && MessageDispatcher.HasDispatcher(message.GetType()) == false)
            {
                throw new ProtocolNotFoundException(message.GetType().Name + " dispatcher not found");
            }
#endif
            MessageDispatcher.InvokeUnsafe(this, message);
            if (bgPolling) MessageDispatcher.Invoke(this, message);
            else lock (mq) mq.Enqueue(new MessageProcessor { Message = message });
        }

        private void Session_Closed(Session session, bool closedByPeer)
        {
            if (bgPolling && Disconnected != null) Disconnected(this, closedByPeer);
            else if (bgPolling == false)
            {
                var mp = new MessageProcessor { Type = closedByPeer ? MessageProcessor.MessageType.DISCONNECTED_BYPEER : MessageProcessor.MessageType.DISCONNECTED };
                lock (mq) mq.Enqueue(mp);
            }
        }
        #endregion

        private TcpClient channel = new TcpClient();
        private Queue<MessageProcessor> mq = new Queue<MessageProcessor>();
        private bool bgPolling;

        private class MessageProcessor
        {
            public enum MessageType
            {
                NORMAL = 0,
                CONNECTED,
                CONNECTED_FAILED,
                DISCONNECTED,
                DISCONNECTED_BYPEER,
                EXCEPTION // 이 경우 Message 는 exception instance
            }
            public MessageType Type = MessageType.NORMAL;
            public object Message;
        }
    }
}
